<?php
$this->load->View('include/header.php');

if ($set == "rfid") {
?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data RFID
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-credit-card"></i> RFID</a></li>
        <li class="active">Data RFID</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php echo "<br>";
              echo $this->session->flashdata('pesan'); ?>
              <br>
              <a href="<?php base_url() ?>add_rfid"><button type="button" class="btn btn-primary btn-md"><i class="glyphicon glyphicon-plus"></i> Tambah RFID</button></a>
              <!-- Button trigger modal -->
              <?php if (form_error('fileURL')) { ?>
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <?php print form_error('fileURL'); ?>
                </div>
              <?php } ?>
              <br>
              <br>
              <div class="btn-group mb-3 btn-group-sm">
                <a class="btn btn-primary" data-toggle="modal" data-target="#importModal">Import</a>
                <a href="<?= base_url('admin/export_rfid') ?>" type="button" class="btn btn-success">Export</a>
              </div>
              <?php if (form_error('fileURL')) { ?>
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <?php print form_error('fileURL'); ?>
                </div>
              <?php } ?>

              <!-- Modal -->
              <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="importLabel">Modal title</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body m-3">
                      <form action="<?= base_url('admin/upload_rfid') ?>" enctype="multipart/form-data" method="post">
                        <input class="form-control" type="file" id="file-import-siswa" name="fileURL">
                        <button type="submit" class="btn btn-sm btn-primary">Import</button>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <a href="<?= base_url('admin/download_rfid_template') ?>" class="btn btn-primary">Download Template</a>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              <br><br><br>
              <h1 class="box-title">Data RFID</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="t1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">UID RFID</th>
                    <th style="text-align:center">Nama</th>
                    <th style="text-align:center">NIK</th>
                    <th style="text-align:center">Telp</th>
                    <th style="text-align:center">Gender</th>
                    <th style="text-align:center">Alamat</th>
                    <th style="text-align:center">Opsi</th>
                    <th style="text-align:center">Status</th>
                    <th style="text-align:center">Periode</th>
                    <th style="text-align:center">Jadwal</th>
                    <th style="text-align:center">#</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (empty($rfid)) { ?>
                    <tr>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                      <td>Data tidak ditemukan</td>
                    </tr>
                    <?php } else {
                    $no = 0;
                    foreach ($rfid as $row) {

                      $no++; ?>
                      <tr>
                        <td style="text-align:center"><?php echo $no; ?></td>
                        <td style="text-align:center"><b class="text-success"><?php echo $row->uid; ?></b></td>
                        <td style="text-align:center"><?php echo $row->nama; ?></td>
                        <td style="text-align:center"><?php echo $row->nik; ?></td>
                        <td style="text-align:center"><?php echo $row->telp; ?></td>
                        <td style="text-align:center"><?php echo $row->gender; ?></td>
                        <td style="text-align:center"><?php echo $row->alamat; ?></td>
                        <td style="text-align:center"><?php echo $row->opsi; ?></td>
                        <td style="text-align:center"><?php echo $row->status; ?></td>
                        <td style="text-align:center"><?php echo $row->periode; ?></td>
                        <td style="text-align:center"><?php if ($row->periode == 'perminggu') {
                                                        echo $row->jadwal_hari;
                                                      } else {
                                                        echo $row->jadwal_tanggal;
                                                      } ?></td>
                        <td style="text-align:center">
                          <a href="<?= base_url() ?>/admin/edit_rfid/<?= $row->id_rfid ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
                          <!-- <a href="<?php site_url() ?>/admin/hapus_rfid/<?= $row->id_rfid ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a> -->
                        </td>
                      </tr>
                  <?php
                    }
                  } ?>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php
} else if ($set == "add-rfid") {
?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah RFID
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-gears"></i> Data RFID</a></li>
        <li class="active">Tambah RFID</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php echo "<br>";
              echo $this->session->flashdata('pesan'); ?>
              <br>
              <h1 class="box-title">Tambah RFID</h1>
            </div>
            <!-- /.box-header -->
            <form role="form" action="<?= base_url(); ?>/admin/save_rfid" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" placeholder="nama" required>
                  <span class="text-danger">
                    <?= form_error('nama') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>UID</label>
                  <input type="text" name="uid" class="form-control" placeholder="uid" required>
                  <span class="text-danger">
                    <?= form_error('uid') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>NIK</label>
                  <input type="number" name="nik" class="form-control" placeholder="nik" required>
                  <span class="text-danger">
                    <?= form_error('nik') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Telp</label>
                  <input type="number" name="telp" class="form-control" placeholder="telp" required>
                  <span class="text-danger">
                    <?= form_error('telp') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Gender</label>
                  <input type="text" name="gender" class="form-control" placeholder="gender" required>
                  <span class="text-danger">
                    <?= form_error('gender') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Periode</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="periode" id="perbulan" value="perbulan">
                    <label class="form-check-label" for="perbulan">
                      Perbulan
                    </label>
                    <input class="form-check-input" type="radio" name="periode" id="perminggu" value="perminggu">
                    <label class="form-check-label" for="perminggu">
                      Perminggu
                    </label>
                  </div>
                  <span class="text-danger">
                    <?= form_error('periode') ?>
                  </span>
                </div>

                <div class="form-group">
                  <label>Jadwal</label>
                  <select name="jadwal_tanggal" id="jadwal_tanggal" class="form-control" placeholder="jadwal_tanggal">
                    <option selected disabled>Pilih Jadwal Perbulan</option>
                    <?php foreach ($dates as $date) : ?>
                      <option value=<?= $date ?>><?= $date ?></option>
                    <?php endforeach ?>
                  </select>
                  <span class="text-danger">
                    <?= form_error('jadwal_tanggal') ?>
                  </span>
                  <select name="jadwal_hari" id="jadwal_hari" class="form-control" placeholder="jadwal_hari">
                    <option selected disabled>Pilih Jadwal Perminggu</option>
                    <?php foreach ($days as $day) : ?>
                      <option value=<?= $day ?>><?= $day ?></option>
                    <?php endforeach ?>
                  </select>
                  <span class="text-danger">
                    <?= form_error('jadwal_hari') ?>
                  </span>
                </div>

                <div class="form-group">
                  <label>Opsi</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" value="3kg" name="opsi" id="3kg">
                    <label class="form-check-label" for="3kg">
                      3kg
                    </label>
                    <input class="form-check-input" type="radio" value="5kg" name="opsi" id="5kg">
                    <label class="form-check-label" for="5kg">
                      5kg
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat" class="form-control" placeholder="alamat" required></textarea>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php
} else if ($set == "edit-rfid") {
?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Data RFID
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-credit-card"></i> Data RFID</a></li>
        <li class="active">Edit Data RFID</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php echo $this->session->flashdata('pesan'); ?>
              <h1 class="box-title"></h1>
            </div>
            <!-- /.box-header -->
            <form role="form" action="<?= base_url(); ?>admin/save_edit_rfid" method="post">
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" name="id" value="<?php if (isset($id)) {
                                                          echo $id;
                                                        } ?>">
                  <!-- <label>ID Device</label>
                  <input type="number" name="id" class="form-control" placeholder="Enter id" required> -->
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" placeholder="nama" value="<?php if (isset($nama)) {
                                                                                                  echo $nama;
                                                                                                } ?>" required>
                  <span class="text-danger">
                    <?= form_error('nama') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>NIK</label>
                  <input type="number" name="nik" class="form-control" placeholder="nik" value="<?php if (isset($nik)) {
                                                                                                  echo $nik;
                                                                                                } ?>" required>
                  <span class="text-danger">
                    <?= form_error('nik') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>UID</label>
                  <input type="text" name="uid" class="form-control" placeholder="uid" value="<?php if (isset($uid)) {
                                                                                                echo $uid;
                                                                                              } ?>" required>
                  <span class="text-danger">
                    <?= form_error('uid') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Telp</label>
                  <input type="text" name="telp" class="form-control" placeholder="telp" value="<?php if (isset($telp)) {
                                                                                                  echo $telp;
                                                                                                } ?>" required>
                  <span class="text-danger">
                    <?= form_error('telp') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Gender</label>
                  <input type="text" name="gender" class="form-control" placeholder="gender" value="<?php if (isset($gender)) {
                                                                                                      echo $gender;
                                                                                                    } ?>" required>
                  <span class="text-danger">
                    <?= form_error('gender') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Periode</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="periode" id="perbulan" value="perbulan" <?php if ($periode == "perbulan") {
                                                                                                                  echo 'checked';
                                                                                                                } ?>>
                    <label class="form-check-label" for="perbulan">
                      Perbulan
                    </label>
                    <input class="form-check-input" type="radio" name="periode" id="perminggu" value="perminggu" <?php if ($periode == "perminggu") {
                                                                                                                    echo 'checked';
                                                                                                                  } ?>>
                    <label class="form-check-label" for="perminggu">
                      Perminggu
                    </label>
                  </div>
                  <span class="text-danger">
                    <?= form_error('periode') ?>
                  </span>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea type="text" name="alamat" class="form-control" placeholder="alamat" required><?php if (isset($alamat)) {
                                                                                                            echo $alamat;
                                                                                                          } ?></textarea>
                </div>
                <div class="form-group">
                  <label>Jadwal</label>
                  <select name="jadwal_tanggal" id="jadwal_tanggal" class="form-control" placeholder="jadwal_tanggal">
                    <option selected disabled>Pilih Jadwal Perbulan</option>
                    <?php foreach ($dates as $date) : ?>
                      <option value=<?= $date ?> <?php if ($jadwal_tanggal == $date) {
                                                    echo 'selected';
                                                  } ?>><?= $date ?></option>
                    <?php endforeach ?>
                  </select>
                  <span class="text-danger">
                    <?= form_error('jadwal_tanggal') ?>
                  </span>
                  <select name="jadwal_hari" id="jadwal_hari" class="form-control" placeholder="jadwal_hari">
                    <option selected disabled>Pilih Jadwal Perminggu</option>
                    <?php foreach ($days as $day) : ?>
                      <option value=<?= $day ?> <?php if ($jadwal_hari == $day) {
                                                  echo 'selected';
                                                } ?>><?= $day ?></option>
                    <?php endforeach ?>
                  </select>
                  <span class="text-danger">
                    <?= form_error('jadwal_hari') ?>
                  </span>
                </div>

                <div class="form-group">
                  <label>Opsi</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" value="3kg" name="opsi" id="3kg" <?php if (isset($opsi) == '3kg') {
                                                                                                    echo 'checked';
                                                                                                  } ?>>
                    <label class="form-check-label" for="3kg">
                      3kg
                    </label>
                    <input class="form-check-input" type="radio" value="5kg" name="opsi" id="5kg" <?php if (isset($opsi) == '5kg') {
                                                                                                    echo 'checked';
                                                                                                  } ?>>
                    <label class="form-check-label" for="5kg">
                      5kg
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="status" id="0" value="0" <?php if ($status == '0') {
                                                                                                  echo 'checked';
                                                                                                } ?>>
                    <label class="form-check-label" for="0">
                      0
                    </label>
                    <input class="form-check-input" type="radio" name="status" id="1" value="1" <?php if ($status == '1') {
                                                                                                  echo 'checked';
                                                                                                } ?>>
                    <label class="form-check-label" for="1">
                      1
                    </label>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php
}

$this->load->view('include/footer.php');
?>

</div> <!-- penutup header -->

<!-- jQuery 3 -->
<script src="<?= base_url(); ?>components/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>components/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>components/dist/js/adminlte.min.js"></script>

<!-- DataTables -->
<script src="<?= base_url(); ?>components/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>components/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function() {
    $("#t1").DataTable();
  });
</script>

</body>

</html>