<?php
if ($set == "dashboard") {
  $this->load->View('include/header.php');

  $jmlrfid = 0;
  $jmlalat = 0;
  $jmlmasuk = 0;
  $jmlkeluar = 0;

  if (isset($rfid)) {
    foreach ($rfid as $key => $value) {
      $jmlrfid++;
    }
  }

  if (isset($devices)) {
    foreach ($devices as $key => $value) {
      $jmlalat++;
    }
  }

  if (isset($masuk)) {
    foreach ($masuk as $key => $value) {
      $jmlmasuk++;
    }
  }

  if (isset($keluar)) {
    foreach ($keluar as $key => $value) {
      $jmlkeluar++;
    }
  }
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <!-- <li class="active">Dashboard</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->

      <div class="row">
        <!-- /.col -->


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $jmlrfid; ?></h3>

              <p>RFID CARD</p>
            </div>
            <div class="icon">
              <i class="ion ion-card"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $jmlalat; ?></h3>

              <p>Alat</p>
            </div>
            <div class="icon">
              <i class="ion ion-wifi"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $jmlPengambilanHariIni; ?></h3>

              <p>Pengambilan Hari ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $jmlPengambilanBulanIni; ?></h3>

              <p>Pengambilan Bulan ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <div class="col-lg-12 col-xs-12">
          <div class="card">
            <div class="box box-primary">
              <div class="box-body">
                <form action="<?= base_url(); ?>admin/dashboard" method="get">
                  <div class="form-group col-md-4 col-lg-6">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="tanggal" class="form-control pull-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">Ambil Data Pengambilan</button>
                  </div>
                </form>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div>
                  <canvas id="PengambilanChart"></canvas>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
}

$this->load->view('include/footer.php');
?>

<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>


<!-- ./wrapper -->
<!-- ChartJS -->
<script>
  var datas = JSON.parse('<?= $jmlPengambilanMingguIni ?>')
  var days = []
  var jmlh = []
  datas.forEach(e => {
    days.push(e.tanggal)
    jmlh.push(e.total)
  })
  var ctx = document.getElementById("PengambilanChart").getContext('2d');
  var PengambilanChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: days,
      datasets: [{
        label: '# Pengambilan',
        data: jmlh,
        backgroundColor: 'RoyalBlue',
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>
<!-- jQuery 3 -->
<script src="<?= base_url(); ?>components/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>components/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>components/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>components/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>components/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>components/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url(); ?>components/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url(); ?>components/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Sparkline -->
<script src="<?= base_url(); ?>components/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?= base_url(); ?>components/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url(); ?>components/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>components/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url(); ?>components/bower_components/Chart.js/Chart.js"></script>
<script>
  $(function() {
    $("#t1").DataTable();
    $('#t2').DataTable();
  });

  $(function() {
    //Date range picker
    $('#reservation').daterangepicker()

  })
</script>

</body>

</html>