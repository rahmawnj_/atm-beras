<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->load->library('bcrypt');
		$this->load->library(["form_validation", 'session']);
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		redirect(base_url() . 'admin/dashboard');
	}

	public function dashboard()
	{

		if ($this->input->get('tanggal')) {
			$tgl = $this->input->get('tanggal');
			//echo $tgl;
			$days = explode("-", $tgl);
			$time1 = strtotime($days[0]);
			$date1 = date("Y-m-d", $time1);;
			$time2 = strtotime($days[1]);
			$date2 = date("Y-m-d", $time2);;
			$jmlPengambilanMingguIni = $this->m_admin->get_pengambilan2($date1, $date2);
		} else {
			$day = date('w');
			$week_start = date('Y-m-d', strtotime('-' . $day . ' days'));
			$week_end = date('Y-m-d', strtotime('+' . (6 - $day) . ' days'));
			$jmlPengambilanMingguIni = $this->m_admin->get_pengambilan2($week_start, $week_end);
		}

		$data['jmlPengambilanMingguIni'] = json_encode($jmlPengambilanMingguIni);

		$data['set'] = "dashboard";
		$data['rfid'] = $this->m_admin->get_rfid();
		$data['devices'] = $this->m_admin->get_devices();
		// Hari Ini
		$today = strtotime("today");
		$tomorrow = strtotime("tomorrow");
		if ($this->m_admin->get_pengambilan($today, $tomorrow) == null) {
			$pengambilanHariIni = 0;
		} else {
			$pengambilanHariIni = count($this->m_admin->get_pengambilan($today, $tomorrow));
		}
		$data['jmlPengambilanHariIni'] = $pengambilanHariIni;
		$firstDayTM = mktime(0, 0, 0, date("m"), 1, date("Y"));
		$lastDayTM = mktime(0, 0, 0, date("m"), date('t'), date("Y"));
		if ($this->m_admin->get_pengambilan($firstDayTM, $lastDayTM) == null) {
			$pengambilanBulanIni = 0;
		} else {
			$pengambilanBulanIni = count($this->m_admin->get_pengambilan($firstDayTM, $lastDayTM));
		}
		$data['jmlPengambilanBulanIni'] = $pengambilanBulanIni;

		$this->load->view('v_dashboard', $data);
	}

	public function list_users()
	{
		$data['set'] = "list-users";
		$data['data'] = $this->m_admin->get_users();
		$this->load->view('v_users', $data);
	}

	public function tes()
	{
		$data['set'] = "add-users";
		$this->load->view('tes', $data);
	}
	public function add_users()
	{
		$data['set'] = "add-users";
		$this->load->view('v_users', $data);
	}


	public function save_users()
	{
		if ($this->session->userdata('userlogin')) {
			$this->form_validation->set_rules('users', 'Username', 'required|trim');
			$this->form_validation->set_rules('pass', 'Password', 'required|trim');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]|trim');
			$this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.email]|trim');
			if ($this->form_validation->run() == false) {
				$this->add_users();
			} else {
				$users = $this->input->post('users');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$pass = $this->input->post('pass');
				$hash = $this->bcrypt->hash_password($pass);

				$type = explode('.', $_FILES["image"]["name"]);
				$type = strtolower($type[count($type) - 1]);
				$imgname = uniqid(rand()) . '.' . $type;
				$url = "components/dist/img/" . $imgname;
				if (in_array($type, array("jpg", "jpeg", "gif", "png"))) {
					if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
						if (move_uploaded_file($_FILES["image"]["tmp_name"], $url)) {
							$data = array(
								'nama'    => $users,
								'email'   => $email,
								'username' => $username,
								'password' => $hash,
								'avatar'  => $imgname,
							);
							$this->m_admin->insert_users($data);
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
						}
					}
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan, ekstensi gambar salah</div>");
				}
				redirect(base_url() . 'admin/list_users');
			}
		}
	}


	public function hapus_users($id = null)
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{
			$path = "";
			$filename = $this->m_admin->get_user_byid($id);
			foreach ($filename as $key) {
				$file = $key->avatar;
				$path = "components/dist/img/" . $file;
			}

			//echo $path;

			if (file_exists($path)) {
				unlink($path);
				if ($this->m_admin->users_del($id)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}
			} else {
				if ($this->m_admin->users_del($id)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus image gagal dihapus</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}
			}

			redirect(base_url() . 'admin/list_users');
		}
	}


	public function edit_users($id = null)
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($id)) {
				$user = $this->m_admin->get_user_byid($id);
				foreach ($user as $key => $value) {
					//print_r($value);
					$data['id'] = $id;
					$data['nama'] = $value->nama;
					$data['email'] = $value->email;
					$data['username'] = $value->username;
					$data['password'] = $value->password;
					$data['avatar'] = $value->avatar;
				}
				$data['set'] = "edit-users";
				$this->load->view('v_users', $data);
			} else {
				redirect(base_url() . 'admin/list_users');
			}
		}
	}

	public function save_edit_users()
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($_POST['id']) && isset($_POST['email'])) {
				$id = $this->input->post('id');
				$email = $this->input->post('email');
				$nama = $this->input->post('users');
				$username = $this->input->post('username');
				$pass = $this->input->post('pass');
				$hash = $this->bcrypt->hash_password($pass);


				$type = explode('.', $_FILES["image"]["name"]);
				$type = strtolower($type[count($type) - 1]);
				$imgname = uniqid(rand()) . '.' . $type;
				$url = "components/dist/img/" . $imgname;
				if (in_array($type, array("jpg", "jpeg", "gif", "png"))) {
					if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
						if (move_uploaded_file($_FILES["image"]["tmp_name"], $url)) {
							$data = array(
								'nama'    => $nama,
								'email'   => $email,
								'username' => $username,
								'avatar'  => $imgname,
							);
							$file = $this->input->post('img');
							$path = "components/dist/img/" . $file;

							if (file_exists($path)) {
								unlink($path);
							}
							$this->m_admin->updateUser($id, $data);
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
						}
					}
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan, ekstensi gambar salah</div>");
				}

				if (isset($_POST['changepass'])) {
					$data = array(
						'email' => $email,
						'nama' => $nama,
						'username' => $username,
						'password' => $hash,
					);
					if ($this->m_admin->updateUser($id, $data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}
				} else {
					$data = array(
						'email' => $email,
						'nama' => $nama,
						'username' => $username,
					);
					if ($this->m_admin->updateUser($id, $data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}
				}

				redirect(base_url() . 'admin/list_users');
			}
		}
	}


	public function devices()
	{
		$data['set'] = "devices";
		$data['devices'] = $this->m_admin->get_devices();

		$this->load->view('v_devices', $data);
	}

	public function add_devices()
	{
		$data['set'] = "add-devices";
		$this->load->view('v_devices', $data);
	}

	public function save_devices()
	{
		if ($this->session->userdata('userlogin')) {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');

			//$duplicate = $this->m_admin->get_devices_byid_row($id);
			//$hasil = count($duplicate);


			if (false) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> ID Alat sudah terdaftar, ganti ID Alat</div>");
			} else {
				$data = array(
					'nama_devices'  => $nama, 'mode'  => 'SCAN',
				);

				if ($this->m_admin->insert_devices($data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan</div>");
				}
			}

			redirect(base_url() . 'admin/devices');
		}
	}

	public function hapus_devices($id = null)
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{
			if ($this->m_admin->devices_del($id)) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
			}

			redirect(base_url() . 'admin/devices');
		}
	}

	public function edit_devices($id = null)
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($id)) {

				$devices = $this->m_admin->get_devices_byid($id);
				if (isset($devices)) {
					foreach ($devices as $key => $value) {
						//print_r($value);
						$data['id'] = $value->id_devices;
						$data['nama_devices'] = $value->nama_devices;
					}
					$data['set'] = "edit-devices";
					$this->load->view('v_devices', $data);
				}
			} else {
				redirect(base_url() . 'admin/devices');
			}
		}
	}

	public function edit_devices_mode($id = null)
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($id)) {

				$devices = $this->m_admin->get_devices_byid($id);
				if (isset($devices)) {
					foreach ($devices as $key => $value) {
						//print_r($value);
						$data['id'] = $value->id_devices;
						$data['mode'] = $value->mode;
					}
					$data['set'] = "edit-devices-mode";
					$this->load->view('v_devices', $data);
				}
			} else {
				redirect(base_url() . 'admin/devices');
			}
		}
	}

	public function save_edit_devices()
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($_POST['id']) && isset($_POST['nama'])) {
				$id = $this->input->post('id');
				$nama = $this->input->post('nama');

				$data = array(
					'nama_devices' => $nama,
				);

				if ($this->m_admin->updateDevices($id, $data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
				}
				redirect(base_url() . 'admin/devices');
			}
		}
	}

	public function save_edit_devices_mode()
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			$id = $this->input->post('id');
			$mode = $this->input->post('mode');

			if ($mode) {
				$data = array('mode' => 'ADD',);
			} else {
				$data = array('mode' => 'SCAN',);
			}


			if ($this->m_admin->updateDevices($id, $data)) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Mode berhasil di update</div>");
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Mode gagal di update</div>");
			}
			redirect(base_url() . 'admin/devices');
		}
	}


	public function histori()
	{
		$data['set'] = "histori";
		$data['histori'] = $this->m_admin->get_history();

		$this->load->view('v_histori', $data);
	}


	public function hapus_histori()
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{

			if ($this->m_admin->empty_data()) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Histori berhasil di hapus</div>");
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Histori gagal di hapus</div>");
			}

			redirect(base_url() . 'admin/histori');
		}
	}

	public function rfid()
	{
		$data['set'] = "rfid";
		$data['rfid'] = $this->m_admin->get_rfid();

		$this->load->view('v_rfid', $data);
	}

	public function add_rfid()
	{
		$data['days'] = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu'];
		$data['dates'] = [
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
			16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
		];
		$data['set'] = "add-rfid";
		$this->load->view('v_rfid', $data);
	}

	public function save_rfid()
	{
		if ($this->session->userdata('userlogin')) {
			$this->form_validation->set_rules('uid', 'RFID', 'required|is_unique[rfid.uid]|trim');
			$this->form_validation->set_rules('nik', 'NIK', 'required|is_unique[rfid.nik]|trim');
			$this->form_validation->set_rules('telp', 'Telp', 'required|trim');
			$this->form_validation->set_rules('nama', 'Name', 'required|trim');
			$this->form_validation->set_rules('gender', 'Gender', 'required|trim');
			$this->form_validation->set_rules('periode', 'Periode', 'required|trim');
			if ($this->input->post('periode') == 'perbulan') {
				$this->form_validation->set_rules('jadwal_tanggal', 'Jadwal Perbulan', 'required|trim');
			} else {
				$this->form_validation->set_rules('jadwal_hari', 'Jadwal Perminggu', 'required|trim');
			}
			if ($this->form_validation->run() == false) {
				$this->add_rfid();
			} else {
				$nama = $this->input->post('nama');
				$nik = $this->input->post('nik');
				$uid = $this->input->post('uid');
				$telp = $this->input->post('telp');
				$gender = $this->input->post('gender');
				$jadwal_tanggal = $this->input->post('jadwal_tanggal');
				$jadwal_hari = $this->input->post('jadwal_hari');
				$periode = $this->input->post('periode');
				$alamat = $this->input->post('alamat');
				$opsi = $this->input->post('opsi');
				$status = 0;

				if (false) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> ID Alat sudah terdaftar, ganti ID Alat</div>");
				} else {
					$data = array(
						'nama'  => $nama,
						'nik'  => $nik,
						'telp'  => $telp,
						'uid'  => $uid,
						'gender'  => $gender,
						'jadwal_tanggal'  => $jadwal_tanggal,
						'jadwal_hari'  => $jadwal_hari,
						'periode'  => $periode,
						'opsi'  => $opsi,
						'alamat'  => $alamat,
						'status'  => $status,
					);

					if ($this->m_admin->insert_rfid($data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan</div>");
					}
				}

				redirect(base_url() . 'admin/rfid');
			}
		}
	}


	public function edit_rfid($id = null)
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($id)) {
				$rfid = $this->m_admin->get_rfid_byid($id);
				if (isset($rfid)) {
					foreach ($rfid as $key => $value) {
						//print_r($value);
						$data['id'] = $value->id_rfid;
						$data['nama'] = $value->nama;
						$data['nik'] = $value->nik;
						$data['uid'] = $value->uid;
						$data['telp'] = $value->telp;
						$data['jadwal_hari'] = $value->jadwal_hari;
						$data['jadwal_tanggal'] = $value->jadwal_tanggal;
						$data['status'] = $value->status;
						$data['opsi'] = $value->opsi;
						$data['gender'] = $value->gender;
						$data['alamat'] = $value->alamat;
						$data['periode'] = $value->periode;
					}
					$data['set'] = "edit-rfid";
					$data['days'] = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu'];
					$data['dates'] = [
						1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
						16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
					];
					$this->load->view('v_rfid', $data);
				} else {
					redirect(base_url() . 'admin/rfid/datarfid');
				}
			} else {
				redirect(base_url() . 'admin/rfid/datarfid');
			}
		}
	}

	public function save_edit_rfid()
	{

		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login

			$this->form_validation->set_rules('telp', 'Telp', 'required|trim');
			$this->form_validation->set_rules('nama', 'Name', 'required|trim');
			$this->form_validation->set_rules('gender', 'Gender', 'required|trim');
			$this->form_validation->set_rules('periode', 'Periode', 'required|trim');
			if ($this->input->post('periode') == 'perbulan') {
				$this->form_validation->set_rules('jadwal_tanggal', 'Jadwal Perbulan', 'required|trim');
			} else {
				$this->form_validation->set_rules('jadwal_hari', 'Jadwal Perminggu', 'required|trim');
			}
			$rfid = $this->m_admin->get_rfid_byid($this->input->post('id'))[0];
			if ($this->input->post('uid') == $rfid->uid) {
				$this->form_validation->set_rules('uid', 'RFID', 'required|trim');
			} else {
				$this->form_validation->set_rules('uid', 'RFID', 'required|is_unique[rfid.uid]|trim');
			}
			if ($this->input->post('nik') == $rfid->nik) {
				$this->form_validation->set_rules('nik', 'NIK', 'required|trim');
			} else {
				$this->form_validation->set_rules('nik', 'NIK', 'required|is_unique[rfid.nik]|trim');
			}
			if ($this->form_validation->run() == false) {
				$this->edit_rfid($this->input->post('id'));
			} else {
				if (isset($_POST['id']) && isset($_POST['nama'])) {
					$id = $this->input->post('id');
					$nama = $this->input->post('nama');
					$nik = $this->input->post('nik');
					$uid = $this->input->post('uid');
					$jadwal_hari = $this->input->post('jadwal_hari');
					$jadwal_tanggal = $this->input->post('jadwal_tanggal');
					$periode = $this->input->post('periode');
					$alamat = $this->input->post('alamat');
					$telp = $this->input->post('telp');
					$gender = $this->input->post('gender');
					$opsi = $this->input->post('opsi');
					$status = $this->input->post('status');

					$data = array(
						'nama'  => $nama,
						'nik'  => $nik,
						'telp'  => $telp,
						'uid'  => $uid,
						'gender'  => $gender,
						'jadwal_hari'  => $jadwal_hari,
						'jadwal_tanggal'  => $jadwal_tanggal,
						'periode'  => $periode,
						'opsi'  => $opsi,
						'alamat'  => $alamat,
						'status'  => $status,
					);

					if ($this->m_admin->updateRFID($id, $data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}
					redirect(base_url() . 'admin/rfid');
				}
			}
		}
	}


	public function hapus_rfid($id = null)
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{
			if ($this->m_admin->rfid_del($id)) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
			}

			redirect(base_url() . 'admin/rfid/datarfid');
		}
	}

	public function download_rfid_template()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		// set Header
		$sheet->SetCellValue('A1', 'uid')->getColumnDimension('A')->setAutoSize(true);
		$sheet->SetCellValue('B1', 'nama')->getColumnDimension('B')->setAutoSize(true);
		$sheet->SetCellValue('C1', 'nik')->getColumnDimension('C')->setAutoSize(true);
		$sheet->SetCellValue('D1', 'telp')->getColumnDimension('D')->setAutoSize(true);
		$sheet->SetCellValue('E1', 'alamat')->getColumnDimension('E')->setAutoSize(true);
		$sheet->SetCellValue('F1', 'gender')->getColumnDimension('F')->setAutoSize(true);
		$sheet->SetCellValue('G1', 'periode')->getColumnDimension('I')->setAutoSize(true);
		$sheet->SetCellValue('H1', 'jadwal_hari')->getColumnDimension('G')->setAutoSize(true);
		$sheet->SetCellValue('I1', 'jadwal_tanggal')->getColumnDimension('H')->setAutoSize(true);
		$sheet->SetCellValue('J1', 'opsi')->getColumnDimension('J')->setAutoSize(true);

		$sheet->SetCellValue('A2', '1a1-b2b-3c3')->getColumnDimension('A')->setAutoSize(true);
		$sheet->SetCellValue('B2', 'Putra')->getColumnDimension('B')->setAutoSize(true);
		$sheet->SetCellValue('C2', '123456789010')->getColumnDimension('C')->setAutoSize(true);
		$sheet->SetCellValue('D2', '081234567890')->getColumnDimension('D')->setAutoSize(true);
		$sheet->SetCellValue('E2', 'Jakarta RW 00/RW 00')->getColumnDimension('E')->setAutoSize(true);
		$sheet->SetCellValue('F2', 'laki-laki')->getColumnDimension('F')->setAutoSize(true);
		$sheet->SetCellValue('G2', 'perminggu')->getColumnDimension('G')->setAutoSize(true);
		$sheet->SetCellValue('H2', 'senin')->getColumnDimension('H')->setAutoSize(true);
		$sheet->SetCellValue('I2', '')->getColumnDimension('I')->setAutoSize(true);
		$sheet->SetCellValue('J2', '3kg')->getColumnDimension('J')->setAutoSize(true);

		$sheet->SetCellValue('A3', '2b2-c3c-4d4')->getColumnDimension('A')->setAutoSize(true);
		$sheet->SetCellValue('B3', 'Putri')->getColumnDimension('B')->setAutoSize(true);
		$sheet->SetCellValue('C3', '123456789010')->getColumnDimension('C')->setAutoSize(true);
		$sheet->SetCellValue('D3', '081234567890')->getColumnDimension('D')->setAutoSize(true);
		$sheet->SetCellValue('E3', 'Jakarta RW 00/RW 00')->getColumnDimension('E')->setAutoSize(true);
		$sheet->SetCellValue('F3', 'perempuan')->getColumnDimension('F')->setAutoSize(true);
		$sheet->SetCellValue('G3', 'perbulan')->getColumnDimension('G')->setAutoSize(true);
		$sheet->SetCellValue('H3', '')->getColumnDimension('H')->setAutoSize(true);
		$sheet->SetCellValue('I3', '27')->getColumnDimension('I')->setAutoSize(true);
		$sheet->SetCellValue('J3', '3kg')->getColumnDimension('J')->setAutoSize(true);

		// $sheet->SetCellValue('A4', '*Opsi dipilih dari baris 1 dan diisi menggunakan huruf kecil');
		// $sheet->SetCellValue('A5', '*Jika Periode perbulan, maka yang harus diisi adalah Jadwal Tanggal');
		// $sheet->SetCellValue('A6', '*Jika Periode perminggu, maka yang harus diisi adalah Jadwal Hari (pastikan menggunakan huruf kecil)');

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$filename = 'example-rfid';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 
	}

	public function upload_rfid()
	{
		$data = array();
		// Load form validation library
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
		if ($this->form_validation->run() == false) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i>Import RFID Gagal</div>");
			redirect('admin/rfid');
		} else {
			// If file uploaded
			if (!empty($_FILES['fileURL']['name'])) {
				// get file extension
				$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);

				if ($extension == 'csv') {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} elseif ($extension == 'xlsx') {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
				}
				// file path
				$spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
				$allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

				// array Count
				$arrayCount = count($allDataInSheet);
				$flag = 0;
				$createArray = ['uid', 'nama', 'nik', 'telp', 'gender', 'jadwal_hari', 'jadwal_tanggal', 'alamat', 'periode', 'opsi'];
				$makeArray = ['uid' => 'uid', 'nama' => 'nama', 'nik' => 'nik', 'telp' => 'telp', 'gender' => 'gender', 'jadwal_hari' => 'jadwal_hari', 'jadwal_tanggal' => 'jadwal_tanggal', 'alamat' => 'alamat', 'periode' => 'periode', 'opsi' => 'opsi'];
				$SheetDataKey = [];
				foreach ($allDataInSheet as $dataInSheet) {
					foreach ($dataInSheet as $key => $value) {
						if (in_array(trim($value), $createArray)) {
							$value = preg_replace('/\s+/', '', $value);
							$SheetDataKey[trim($value)] = $key;
						}
					}
				}

				$dataDiff = array_diff_key($makeArray, $SheetDataKey);
				if (empty($dataDiff)) {
					$flag = 1;
				}

				// match excel sheet column
				if ($flag == 1) {
					for ($i = 2; $i <= $arrayCount; $i++) {
						$uid = $SheetDataKey['uid'];
						$nama = $SheetDataKey['nama'];
						$nik = $SheetDataKey['nik'];
						$telp = $SheetDataKey['telp'];
						$gender = $SheetDataKey['gender'];
						$jadwal_hari = $SheetDataKey['jadwal_hari'];
						$jadwal_tanggal = $SheetDataKey['jadwal_tanggal'];
						$periode = $SheetDataKey['periode'];
						$opsi = $SheetDataKey['opsi'];
						$status = 0;
						$alamat = $SheetDataKey['alamat'];

						$uid = filter_var(trim($allDataInSheet[$i][$uid]), FILTER_SANITIZE_STRING);
						$nama = filter_var(trim($allDataInSheet[$i][$nama]), FILTER_SANITIZE_STRING);
						$nik = filter_var(trim($allDataInSheet[$i][$nik]), FILTER_SANITIZE_STRING);
						$telp = filter_var(trim($allDataInSheet[$i][$telp]), FILTER_SANITIZE_STRING);
						$gender = filter_var(trim($allDataInSheet[$i][$gender]), FILTER_SANITIZE_STRING);
						$jadwal_hari = filter_var(trim($allDataInSheet[$i][$jadwal_hari]), FILTER_SANITIZE_STRING);
						$jadwal_tanggal = filter_var(trim($allDataInSheet[$i][$jadwal_tanggal]), FILTER_SANITIZE_STRING);
						$periode = filter_var(trim($allDataInSheet[$i][$periode]), FILTER_SANITIZE_STRING);
						$opsi = filter_var(trim($allDataInSheet[$i][$opsi]), FILTER_SANITIZE_STRING);
						$status = filter_var(trim($allDataInSheet[$i][$status]), FILTER_SANITIZE_STRING);
						$alamat = filter_var(trim($allDataInSheet[$i][$alamat]), FILTER_SANITIZE_STRING);

						$fetchData[] = [
							'uid' => $uid, 'nama' => $nama, 'nik' => $nik,
							'telp' => $telp, 'gender' => $gender, 'jadwal_hari' => $jadwal_hari,
							'jadwal_tanggal' => $jadwal_tanggal, 'periode' => $periode, 'opsi' => $opsi,
							'status' => $status, 'alamat' => $alamat,
						];
					}
					$data['dataInfo'] = $fetchData;
					$this->m_admin->setBatchImport($fetchData);
					$this->m_admin->importData();
				} else {
					echo "Please import correct file, did not match excel sheet column";
				}
				$this->session->set_flashdata('success', 'Siswa Berhasil Ditambahkan!');
				redirect('admin/rfid');
			}
		}
	}

	public function export_rfid()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		// set Header
		$sheet->SetCellValue('A1', 'NIK')->getColumnDimension('A')->setAutoSize(true);
		$sheet->SetCellValue('B1', 'Nama')->getColumnDimension('B')->setAutoSize(true);
		$sheet->SetCellValue('C1', 'UID')->getColumnDimension('C')->setAutoSize(true);
		$sheet->SetCellValue('D1', 'Telepon')->getColumnDimension('D')->setAutoSize(true);
		$sheet->SetCellValue('E1', 'Gender')->getColumnDimension('E')->setAutoSize(true);
		$sheet->SetCellValue('F1', 'Alamat')->getColumnDimension('E')->setAutoSize(true);
		$sheet->SetCellValue('G1', 'Weight')->getColumnDimension('F')->setAutoSize(true);
		$sheet->SetCellValue('H1', 'Periode')->getColumnDimension('F')->setAutoSize(true);
		// set Row
		$rfids = $this->m_admin->get_rfid();

		$rowCount = 2;
		foreach ($rfids as $rfid) {
			$sheet->SetCellValue('A' . $rowCount, $rfid->nik)->getColumnDimension('A')->setAutoSize(true);
			$sheet->SetCellValue('B' . $rowCount, $rfid->nama)->getColumnDimension('B')->setAutoSize(true);
			$sheet->SetCellValue('C' . $rowCount, $rfid->uid)->getColumnDimension('C')->setAutoSize(true);
			$sheet->SetCellValue('D' . $rowCount, $rfid->telp)->getColumnDimension('D')->setAutoSize(true);
			$sheet->SetCellValue('E' . $rowCount, $rfid->gender)->getColumnDimension('E')->setAutoSize(true);
			$sheet->SetCellValue('F' . $rowCount, $rfid->alamat)->getColumnDimension('F')->setAutoSize(true);
			$sheet->SetCellValue('G' . $rowCount, $rfid->opsi)->getColumnDimension('G')->setAutoSize(true);
			$sheet->SetCellValue('H' . $rowCount, $rfid->periode)->getColumnDimension('H')->setAutoSize(true);

			$rowCount++;
		}

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

		$filename = 'data-rfid';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 

	}


	public function pengambilan()
	{
		$data['set'] = "pengambilan";

		$today = strtotime("today");
		$tomorrow = strtotime("tomorrow");

		$data['pengambilan'] = $this->m_admin->get_pengambilan($today, $tomorrow);

		$this->load->view('v_pengambilan', $data);
	}

	public function lastpengambilan()
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{
			if (isset($_POST['tanggal'])) {
				$tgl = $this->input->post('tanggal');
				//echo $tgl;
				$split1 = explode("-", $tgl);
				$x = 0;
				foreach ($split1 as $key => $value) {
					$date[$x] = $value;
					$x++;
				}

				$ts1 = strtotime($date[0]);
				$ts2 = strtotime($date[1]);

				$tgl1 = date("d-M-Y", $ts1);
				$tgl2 = date("d-M-Y", $ts2);

				$ts2 += 86400;	// tambah 1 hari (hitungan detik)

				// $data['tgl1'] = $tgl1;
				// $data['tgl2'] = $tgl2;

				if ($x == 2) {
					$data['pengambilan'] = $this->m_admin->get_pengambilan($ts1, $ts2);
					$data['tanggal'] = $tgl1 . " - " . $tgl2;
					$data['waktupengambilan'] = $tgl1 . "_" . $tgl2;

					$data['set'] = "last-pengambilan";
					$this->load->view('v_pengambilan', $data);
				} else {
					redirect(base_url() . 'admin/pengambilan');
				}
			} else {
				redirect(base_url() . 'admin/pengambilan');
			}
		}
	}


	public function export2excel()
	{
		if ($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{
			if (isset($_GET['tanggal'])) {
				$tanggal = $this->input->get('tanggal');
				//echo $tanggal;

				$split = explode("_", $tanggal);
				$x = 0;
				foreach ($split as $key => $value) {
					$date[$x] = $value;
					$x++;
				}

				$ts1 = strtotime($date[0]);
				$ts2 = strtotime($date[1]);

				$ts2 += 86400;	// tambah 1 hari (hitungan detik)

				$pengambilan = $this->m_admin->get_pengambilan($ts1, $ts2);

				$spreadsheet = new Spreadsheet;

				$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'No')
					->setCellValue('B1', 'Alat')
					->setCellValue('C1', 'Nama')
					->setCellValue('D1', 'Keterangan')
					->setCellValue('E1', 'Waktu');

				$baris = 2;
				$nomor = 1;
				foreach ($pengambilan as $p) {

					$waktu = date("H:i:s d M Y", $p->created_at);

					$spreadsheet->setActiveSheetIndex(0)
						->setCellValue('A' . $baris,  strval($nomor))
						->setCellValue('B' . $baris, $p->nama_devices)
						->setCellValue('C' . $baris, $p->nama)
						->setCellValue('D' . $baris, $p->keterangan)
						->setCellValue('E' . $baris, $waktu);
					$baris++;
					$nomor++;
				}

				$spreadsheet->setActiveSheetIndex(0);
				$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

				$writer = new Xlsx($spreadsheet);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Pengambilan_' . $tanggal . '.xlsx"');
				header('Cache-Control: max-age=0');

				$writer->save('php://output');
			} else {
				redirect(base_url() . 'admin/pengambilan');
			}
		}
	}

	public function setting()
	{
		$data['set'] = "setting";
		$this->load->view('v_setting', $data);
	}

	public function setwaktuoperasional()
	{
		if ($this->session->userdata('userlogin')) {     // mencegah akses langsung tanpa login
			if (isset($_POST['masuk']) && isset($_POST['keluar'])) {
				$masuk = $this->input->post('masuk');
				$keluar = $this->input->post('keluar');

				if (strlen($masuk) == 11 && strlen($keluar) == 11) {
					if ($masuk[2] == ":" && $masuk[5] == "-" && $masuk[8] == ":" && $keluar[2] == ":" && $keluar[5] == "-" && $keluar[8] == ":") {
						$datamasuk = array('waktu_operasional' => $masuk);
						$datakeluar = array('waktu_operasional' => $keluar);

						if ($this->m_admin->updateWaktuOperasional(1, $datamasuk)) {
							$this->m_admin->updateWaktuOperasional(2, $datakeluar);
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
						} else {
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
						}
					} else {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Salah format waktu, contoh 16:00-17:00</div>");
					}
				} else {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Salah format waktu, contoh 16:00-17:00</div>");
				}
				redirect(base_url() . 'admin/setting');
			}
		}
	}
}
