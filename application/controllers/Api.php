<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->load->model('m_api');
		date_default_timezone_set("asia/jakarta");
	}

	public function index()
	{
		echo "REST API for Device";
	}

	public function getmode()
	{
		if (isset($_GET['iddev'])) {

			$iddev = $this->input->get('iddev');

			$data = $this->m_api->getmode($iddev);
			if (isset($data)) {
				$mode = "-";
				foreach ($data as $key => $value) {
					$mode = $value->mode;
				}
				if ($mode == "-") {
					echo "*id-device-tidak-ditemukan*";
				} else {
					echo "*";
					echo $mode;
					echo "*";
				}
			} else {
				echo "*id-device-tidak-ditemukan*";
			}
		} else {
			echo "*salah-param*";
		}
	}

	public function getmodejson()
	{
		if (isset($_GET['iddev'])) {

			$iddev = $this->input->get('iddev');

			$data = $this->m_api->getmode($iddev);
			if (isset($data)) {
				$mode = "-";
				foreach ($data as $key => $value) {
					$mode = $value->mode;
				}
				if ($mode == "-") {
					$array = array('status' => 'warning', 'mode' => $mode, 'ket' => 'id device tidak ditemukan');
					echo json_encode($array);
				} else {
					$array = array('status' => 'success', 'mode' => $mode, 'ket' => 'berhasil');
					echo json_encode($array);
				}
			} else {
				$array = array('status' => 'warning', 'ket' => 'id device tidak ditemukan');
				echo json_encode($array);
			}
		} else {
			$array = array('status' => 'failed', 'ket' => 'salah parameter');
			echo json_encode($array);
		}
	}

	public function addcard()
	{
		if (isset($_GET['iddev']) && isset($_GET['rfid']) && isset($_GET['opsi']) && isset($_GET['periode'])) {

			$iddev = $this->input->get('iddev');
			$rfid = $this->input->get('rfid');
			$opsi = $this->input->get('opsi');
			$periode = $this->input->get('periode');

			$checkDoubleRFID = $this->m_api->checkRFID($rfid);
			$z = 0;
			if (isset($checkDoubleRFID)) {
				foreach ($checkDoubleRFID as $key => $value) {
					$z++;
				}
			}

			if ($z > 0) {
				echo "*RFID-sudah-terdaftar*";
			} else {
				$device = $this->m_api->getdevice($iddev);
				$count = 0;
				foreach ($device as $key => $value) {
					$count++;
				}
				if ($count > 0) {
					$savedata = array('id_devices' => $iddev, 'uid' => $rfid, 'status' => 0, 'opsi' => $opsi, 'periode' =>  $periode);
					if ($this->m_api->insert_rfid($savedata)) {
						$getlastrfid = $this->m_api->last_rfid();
						$idrfid = 0;
						if (isset($getlastrfid)) {
							foreach ($getlastrfid as $key => $value) {
								$idrfid = $value->id_rfid;
							}
						}
						if ($idrfid > 0) {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'ADD RFID CARD', 'waktu' => time(), 'id_devices' => $iddev);
							if ($this->m_api->insert_histori($histori)) {
								echo "*berhasil-tambah-rfid-card*";
							}
						} else {
							echo "*terjadi-kesalahan*";
						}
					}
				} else {
					echo "*id-device-tidak-ditemukan*";
				}
			}
		} else {
			echo "*salah-param*";
		}
	}


	public function addcardjson()
	{
		if (isset($_GET['iddev']) && isset($_GET['rfid']) && isset($_GET['opsi']) && isset($_GET['periode'])) {

			$iddev = $this->input->get('iddev');
			$rfid = $this->input->get('rfid');
			$opsi = $this->input->get('opsi');
			$periode = $this->input->get('periode');

			$checkDoubleRFID = $this->m_api->checkRFID($rfid);
			$z = 0;
			if (isset($checkDoubleRFID)) {
				foreach ($checkDoubleRFID as $key => $value) {
					$z++;
				}
			}

			if ($z > 0) {
				$notif = array('status' => 'failed', 'ket' => 'RFID sudah terdaftar');
				echo json_encode($notif);
			} else {
				$device = $this->m_api->getdevice($iddev);
				$count = 0;
				foreach ($device as $key => $value) {
					$count++;
				}
				if ($count > 0) {
					$savedata = array('id_devices' => $iddev, 'uid' => $rfid, 'status' => 0, 'opsi' => $opsi, 'periode' => $periode);
					if ($this->m_api->insert_rfid($savedata)) {
						$getlastrfid = $this->m_api->last_rfid();
						$idrfid = 0;
						if (isset($getlastrfid)) {
							foreach ($getlastrfid as $key => $value) {
								$idrfid = $value->id_rfid;
							}
						}
						if ($idrfid > 0) {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'ADD RFID CARD', 'waktu' => time(), 'id_devices' => $iddev);
							if ($this->m_api->insert_histori($histori)) {
								$notif = array('status' => 'success', 'ket' => 'berhasil tambah rfid card');
								echo json_encode($notif);
							}
						} else {
							$notif = array('status' => 'failed', 'ket' => 'terjadi kesalahan');
							echo json_encode($notif);
						}
					}
				} else {
					$notif = array('status' => 'failed', 'ket' => 'device tidak ditemukan');
					echo json_encode($notif);
				}
			}
		} else {
			$notif = array('status' => 'failed', 'ket' => 'salah parameter');
			echo json_encode($notif);
		}
	}


	public function pengambilan()
	{
		if (isset($_GET['iddev']) && isset($_GET['rfid'])) {

			$iddev = $this->input->get('iddev');
			$rfid = $this->input->get('rfid');

			$cekrfid = $this->m_api->checkRFID($rfid);
			$countrfid = 0;
			$idrfid = 0;
			foreach ($cekrfid as $key => $value) {
				$countrfid++;
				$idrfid = $value->id_rfid;
			}

			$device = $this->m_api->getdevice($iddev);
			$count = 0;
			foreach ($device as $key => $value) {
				$count++;
			}

			if ($count > 0) {
				if ($countrfid > 0) {
					if ($cekrfid[0]->periode ==  'perminggu') {
						// PERMINGGU
						$date = date('Y-m-d');
						$day = date('D', strtotime($date));
						$dayList = array(
							'Sun' => 'minggu',
							'Mon' => 'senin',
							'Tue' => 'selasa',
							'Wed' => 'rabu',
							'Thu' => 'kamis',
							'Fri' => 'jumat',
							'Sat' => 'sabtu'
						);
						$hari = $dayList[$day];

						if ($cekrfid[0]->jadwal_hari == $hari) {
							if ($cekrfid[0]->status == 0) {
								$dataRfid = [
									'status' => 1
								];
								if ($this->m_api->update_rfid($idrfid, $dataRfid)) {
									$data = array(
										'id_devices' => $iddev, 'id_rfid' => $idrfid,
										'keterangan' => 'berhasil', 'created_at' => time(),
										'tanggal' => date("Y-m-d")
									);
									if ($this->m_api->insert_pengambilan($data)) {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'berhasil', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										echo "*berhasil*";
									} else {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal insert pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										echo "*gagal-insert-pengambilan*";
									}
								} else {
									$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal update rfid', 'waktu' => time(), 'id_devices' => $iddev);
									$this->m_api->insert_histori($histori);

									echo "*gagal-update-rfid*";
								}
							} else {

								$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-sudah diambil', 'waktu' => time(), 'id_devices' => $iddev);
								$this->m_api->insert_histori($histori);

								echo "*sudah-diambil*";
							}
						} else {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-error jadwal pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
							$this->m_api->insert_histori($histori);

							echo "*error-jadwal-pengambilan*";
						}
					} else if ($cekrfid[0]->periode ==  'perbulan') {
						// BULANAN
						$date = date('Y-m-d');
						$getDate = explode('-', $date);
						if ($cekrfid[0]->jadwal_tanggal == $getDate[2]) {
							if ($cekrfid[0]->status == 0) {
								$dataRfid = [
									'status' => 1
								];
								if ($this->m_api->update_rfid($idrfid, $dataRfid)) {
									$data = array(
										'id_devices' => $iddev, 'id_rfid' => $idrfid,
										'keterangan' => 'berhasil', 'created_at' => time(),
									);
									if ($this->m_api->insert_pengambilan($data)) {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'berhasil ', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi, 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);

										echo "*berhasil*";
									} else {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal insert pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);

										echo "*gagal-insert-pengambilan*";
									}
								} else {
									$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal update rfid', 'waktu' => time(), 'id_devices' => $iddev);
									$this->m_api->insert_histori($histori);
									echo "*gagal-update-rfid*";
								}
							} else {

								$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal', 'waktu' => time(), 'id_devices' => $iddev);
								$this->m_api->insert_histori($histori);

								echo "*sudah-diambil*";
							}
						} else {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-error jadwal pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
							$this->m_api->insert_histori($histori);
							echo "*error-jadwal-pengambilan*";
						}
					}
				} else {
					echo "*rfid-tidak-ditemukan*";
				}
			} else {
				echo "*id-device-tidak-ditemukan*";
			}
		} else {
			echo "*salah-param*";
		}
	}

	public function pengambilanJson()
	{
		if (isset($_GET['iddev']) && isset($_GET['rfid'])) {

			$iddev = $this->input->get('iddev');
			$rfid = $this->input->get('rfid');

			$cekrfid = $this->m_api->checkRFID($rfid);
			$countrfid = 0;
			$idrfid = 0;
			foreach ($cekrfid as $key => $value) {
				$countrfid++;
				$idrfid = $value->id_rfid;
			}

			$device = $this->m_api->getdevice($iddev);
			$count = 0;
			foreach ($device as $key => $value) {
				$count++;
			}

			if ($count > 0) {
				if ($countrfid > 0) {
					if ($cekrfid[0]->periode ==  'perminggu') {
						// PERMINGGU
						$date = date('Y-m-d');
						$day = date('D', strtotime($date));
						$dayList = array(
							'Sun' => 'minggu',
							'Mon' => 'senin',
							'Tue' => 'selasa',
							'Wed' => 'rabu',
							'Thu' => 'kamis',
							'Fri' => 'jumat',
							'Sat' => 'sabtu'
						);
						$hari = $dayList[$day];

						if ($cekrfid[0]->jadwal_hari == $hari) {
							if ($cekrfid[0]->status == 0) {
								$dataRfid = [
									'status' => 1
								];
								if ($this->m_api->update_rfid($idrfid, $dataRfid)) {
									$data = array(
										'id_devices' => $iddev, 'id_rfid' => $idrfid,
										'keterangan' => 'berhasil', 'created_at' => time(),
										'tanggal' => date("Y-m-d")
									);
									if ($this->m_api->insert_pengambilan($data)) {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'berhasil', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										$notif = array('status' => 'success', 'ket' => 'berhasil ', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
										echo json_encode($notif);
									} else {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal insert pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										$notif = array('status' => 'failed', 'ket' => 'gagal insert pengambilan', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
										echo json_encode($notif);
									}
								} else {
									$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-gagal update rfid', 'waktu' => time(), 'id_devices' => $iddev);
									$this->m_api->insert_histori($histori);

									$notif = array('status' => 'failed', 'ket' => 'gagal update rfid', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
									echo json_encode($notif);
								}
							} else {

								$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-sudah diambil', 'waktu' => time(), 'id_devices' => $iddev);
								$this->m_api->insert_histori($histori);
								$notif = array('status' => 'failed', 'ket' => 'sudah diambil', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
								echo json_encode($notif);
							}
						} else {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal-error jadwal pengambilan', 'waktu' => time(), 'id_devices' => $iddev);
							$this->m_api->insert_histori($histori);
							$notif = array('status' => 'failed', 'ket' => 'error jadwal pengambilan', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
							echo json_encode($notif);
						}
					} else {
						// BULANAN
						$date = date('Y-m-d');
						$getDate = explode('-', $date);
						if ($cekrfid[0]->jadwal_tanggal == $getDate[2]) {
							if ($cekrfid[0]->status == 0) {
								$dataRfid = [
									'status' => 1
								];
								if ($this->m_api->update_rfid($idrfid, $dataRfid)) {
									$data = array(
										'id_devices' => $iddev, 'id_rfid' => $idrfid,
										'keterangan' => 'berhasil', 'created_at' => time(),
									);
									if ($this->m_api->insert_pengambilan($data)) {
										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'berhasil', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										$notif = array('status' => 'success', 'ket' => 'berhasil ', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
										echo json_encode($notif);
									} else {

										$histori = array('id_rfid' => $idrfid, 'keterangan' => 'berhasil', 'waktu' => time(), 'id_devices' => $iddev);
										$this->m_api->insert_histori($histori);
										$notif = array('status' => 'failed', 'ket' => 'gagal insert pengambilan', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
										echo json_encode($notif);
									}
								} else {

									$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal', 'waktu' => time(), 'id_devices' => $iddev);
									$this->m_api->insert_histori($histori);

									$notif = array('status' => 'failed', 'ket' => 'gagal update rfid', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
									echo json_encode($notif);
								}
							} else {

								$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal', 'waktu' => time(), 'id_devices' => $iddev);
								$this->m_api->insert_histori($histori);

								$notif = array('status' => 'failed', 'ket' => 'sudah diambil', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
								echo json_encode($notif);
							}
						} else {
							$histori = array('id_rfid' => $idrfid, 'keterangan' => 'gagal', 'waktu' => time(), 'id_devices' => $iddev);
							$this->m_api->insert_histori($histori);

							$notif = array('status' => 'failed', 'ket' => 'error jadwal pengambilan', 'nama' => $cekrfid[0]->nama,  'nik' => $cekrfid[0]->nik, 'weight' => $cekrfid[0]->opsi);
							echo json_encode($notif);
						}
					}
				} else {
					$notif = array('status' => 'failed', 'ket' => 'rfid tidak ditemukan');
					echo json_encode($notif);
				}
			} else {
				$notif = array('status' => 'failed', 'ket' => 'id device tidak ditemukan');
				echo json_encode($notif);
			}
		} else {
			$notif = array('status' => 'failed', 'ket' => 'salah parameter');
			echo json_encode($notif);
		}
	}


	public function absensifoto()
	{
		if (isset($_POST['key']) && isset($_POST['iddev']) && isset($_POST['rfid']) && isset($_POST['foto'])) {
			$key = $this->input->post('key');
			$cekkey = $this->m_api->getkey();

			if ($cekkey[0]->key == $key) {
				$iddev = $this->input->post('iddev');
				$rfid = $this->input->post('rfid');
				$foto = $this->input->post('foto');

				$cekrfid = $this->m_api->checkRFID($rfid);
				$countrfid = 0;
				$idrfid = 0;
				foreach ($cekrfid as $key => $value) {
					$countrfid++;
					$idrfid = $value->id_rfid;
				}

				$device = $this->m_api->getdevice($iddev);
				$count = 0;
				foreach ($device as $key => $value) {
					$count++;
				}

				if ($count > 0) {
					if ($countrfid > 0) {
						$lastRFID = $this->m_api->lastRFIDfoto($idrfid);
						if (isset($lastRFID)) {
							foreach ($lastRFID as $key => $value) {
								if ($value->foto == "") {
									$data = array('foto' => $foto);
									if ($this->m_api->update_($value->id_pengambilan, $data)) {
										echo "*berhasil-insert-foto*";
									} else {
										echo "*gagal-insert-foto*";
									}
								} else {
									echo "*sudah-ada-foto*";
								}
							}
						} else {
							echo "*last-absen-error*";
						}
					} else {
						echo "*rfid-tidak-ditemukan*";
					}
				} else {
					echo "*id-device-tidak-ditemukan*";
				}
			} else {
				echo "*salah-secret-key*";
			}
		} else {
			echo "*salah-param*";
		}
	}


	public function absensifotojson()
	{
		if (isset($_POST['key']) && isset($_POST['iddev']) && isset($_POST['rfid']) && isset($_POST['foto'])) {
			$key = $this->input->post('key');
			$cekkey = $this->m_api->getkey();

			if ($cekkey[0]->key == $key) {
				$iddev = $this->input->post('iddev');
				$rfid = $this->input->post('rfid');
				$foto = $this->input->post('foto');

				$cekrfid = $this->m_api->checkRFID($rfid);
				$countrfid = 0;
				$idrfid = 0;
				foreach ($cekrfid as $key => $value) {
					$countrfid++;
					$idrfid = $value->id_rfid;
				}

				$device = $this->m_api->getdevice($iddev);
				$count = 0;
				foreach ($device as $key => $value) {
					$count++;
				}

				if ($count > 0) {
					if ($countrfid > 0) {
						$lastRFID = $this->m_api->lastRFIDfoto($idrfid);
						if (isset($lastRFID)) {
							foreach ($lastRFID as $key => $value) {
								if ($value->foto == "") {
									$data = array('foto' => $foto);
									if ($this->m_api->update_($value->id_pengambilan, $data)) {
										$notif = array('status' => 'success', 'ket' => 'berhasil insert foto');
										echo json_encode($notif);
									} else {
										$notif = array('status' => 'failed', 'ket' => 'gagal insert foto');
										echo json_encode($notif);
									}
								} else {
									$notif = array('status' => 'failed', 'ket' => 'sudah ada foto');
									echo json_encode($notif);
								}
							}
						} else {
							$notif = array('status' => 'failed', 'ket' => 'last absen error');
							echo json_encode($notif);
						}
					} else {
						$notif = array('status' => 'failed', 'ket' => 'rfid tidak ditemukan');
						echo json_encode($notif);
					}
				} else {
					$notif = array('status' => 'failed', 'ket' => 'id device tidak ditemukan');
					echo json_encode($notif);
				}
			} else {
				$notif = array('status' => 'failed', 'ket' => 'salah secret key');
				echo json_encode($notif);
			}
		} else {
			$notif = array('status' => 'failed', 'ket' => 'salah parameter');
			echo json_encode($notif);
		}
	}


	public function realtimehistori()
	{
		$data = $this->m_admin->get_history();
		echo '<table id="t1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="text-align:center">ID Histori</th>
                  <th style="text-align:center">UID RFID</th>
                  <th style="text-align:center">Keterangan</th>
                  <th style="text-align:center">Nama Device</th>
                  <th style="text-align:center">Waktu</th>
                </tr>
                </thead>
                <tbody>';
		if (empty($data)) {
			echo '
                <tr>
                  <td style="text-align:center">Data tidak ditemukan</td>
                  <td style="text-align:center">Data tidak ditemukan</td>
                  <td style="text-align:center">Data tidak ditemukan</td>
                  <td style="text-align:center">Data tidak ditemukan</td>
                  <td style="text-align:center">Data tidak ditemukan</td>
                </tr>';
		} else {
			foreach ($data as $row) {
				echo '
                <tr>
                  <td style="text-align:center"><b class="text-success">' . $row->id_histori . '</b></td>
                  <td style="text-align:center">' . $row->uid . '</td>
                  <td style="text-align:center">' . $row->keterangan . '</td>
                  <td style="text-align:center">' . $row->nama_devices . ' (' . $row->id_devices . ')</td>
                  <td style="text-align:center">' . date("d M Y, H:i:s", $row->waktu) . '</td>
                </tr>';
			}
		}
		echo '
                </tbody>
              </table>';
	}
}
